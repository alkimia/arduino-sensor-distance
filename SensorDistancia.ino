#include "SR04.h"
#define TRIG_PIN 12
#define ECHO_PIN 11
SR04 sr04 = SR04(ECHO_PIN,TRIG_PIN);
long a;

// pin de salida para los leds
const int led_verde_1 = 2;
const int led_verde_2 = 3;
const int led_amarillo_1 = 4;
const int led_amarillo_2 = 5;
const int led_rojo_1 = 6;
const int led_rojo_2 = 7;
const int pin_alarma = 8;

void setup() {
 Serial.begin(9600);
 delay(1000);
     
  // Configuramos los pines de los leds y del zumbador como salidas
  pinMode(led_verde_1, OUTPUT);      
  pinMode(led_verde_2, OUTPUT); 
  pinMode(led_amarillo_1, OUTPUT); 
  pinMode(led_amarillo_2, OUTPUT); 
  pinMode(led_rojo_1, OUTPUT);
  pinMode(led_rojo_2, OUTPUT);
  pinMode(pin_alarma, OUTPUT);

  // prueba leds
  test_leds();
}

void test_leds() {
  digitalWrite(led_verde_1, HIGH);  
  delay(250);
  digitalWrite(led_verde_1, LOW);    
  digitalWrite(led_verde_2, HIGH);  
  delay(250);
  digitalWrite(led_verde_2, LOW);    
  digitalWrite(led_amarillo_1, HIGH);   
  delay(250);
  digitalWrite(led_amarillo_1, LOW);    
  digitalWrite(led_amarillo_2, HIGH);   
  delay(250);
  digitalWrite(led_amarillo_2, LOW);   
  digitalWrite(led_rojo_1, HIGH);   
  delay(250);
  digitalWrite(led_rojo_1, LOW);   
  digitalWrite(led_rojo_2, HIGH);   
  delay(250);
  digitalWrite(led_rojo_2, LOW);   
}

void alarma() {
  digitalWrite(pin_alarma, HIGH);     
  delay(500);
  digitalWrite(pin_alarma, LOW);
  delay(250);
}

void loop() {
  //
   a=sr04.Distance();
 Serial.print(a);
   Serial.println("cm");
      
   if (a>60) {
      digitalWrite(led_verde_1, LOW);        
      digitalWrite(led_verde_2, LOW);        
      digitalWrite(led_amarillo_1, LOW);        
      digitalWrite(led_amarillo_2, LOW);        
      digitalWrite(led_rojo_1, LOW);        
      digitalWrite(led_rojo_2, LOW);   
      alarma();
   } else {
      if (a>0 && a<10) {
        digitalWrite(led_verde_1, HIGH);
        digitalWrite(led_verde_2, HIGH);
        digitalWrite(led_amarillo_1, HIGH);
        digitalWrite(led_amarillo_2, HIGH);
        digitalWrite(led_rojo_1, HIGH);
        digitalWrite(led_rojo_2, HIGH);
        alarma();       
      } else if (a>10 && a<20) {
        digitalWrite(led_verde_1, HIGH);
        digitalWrite(led_verde_2, HIGH);
        digitalWrite(led_amarillo_1, HIGH);
        digitalWrite(led_amarillo_2, HIGH);
        digitalWrite(led_rojo_1, HIGH);
        digitalWrite(led_rojo_2, LOW);
        alarma();
      } else if (a>20 && a<30) {
        digitalWrite(led_verde_1, HIGH);
        digitalWrite(led_verde_2, HIGH);
        digitalWrite(led_amarillo_1, HIGH);
        digitalWrite(led_amarillo_2, HIGH);
        digitalWrite(led_rojo_1, LOW);
        digitalWrite(led_rojo_2, LOW);
        alarma();
      } else if (a>30 && a<40) {
        digitalWrite(led_verde_1, HIGH);
        digitalWrite(led_verde_2, HIGH);
        digitalWrite(led_amarillo_1, HIGH);
        digitalWrite(led_amarillo_2, LOW);
        digitalWrite(led_rojo_1, LOW);
        digitalWrite(led_rojo_2, LOW);
        alarma();
      } else if (a>40 && a<50) {
        digitalWrite(led_verde_1, HIGH);
        digitalWrite(led_verde_2, HIGH);
        digitalWrite(led_amarillo_1, LOW);
        digitalWrite(led_amarillo_2, LOW);
        digitalWrite(led_rojo_1, LOW);
        digitalWrite(led_rojo_2, LOW);
        alarma();
      } else if (a>50 && a<60) {
        digitalWrite(led_verde_1, HIGH);
        digitalWrite(led_verde_2, LOW);
        digitalWrite(led_amarillo_1, LOW);
        digitalWrite(led_amarillo_2, LOW);
        digitalWrite(led_rojo_1, LOW);
        digitalWrite(led_rojo_2, LOW);
        alarma();
       }
   }
   delay(250);  
}
